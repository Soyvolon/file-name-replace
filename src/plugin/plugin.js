const fileNameReplacePlugin = (props) => (hook) =>
  hook.beforeEach((html, next) => {
    if (!props['prefix']) {
      props['prefix'] = 'r'
    }

    let methods = props['methods'];

    let expr = `\\{\\{${props.prefix}\\:(.*)\\}\\:?(.*)?\\}`;
    let regex = new RegExp(expr, 'gi');

    let matches = html.matchAll(regex);

    doMatchReplacementsAsync(html, matches, methods)
      .then(newHtml => {
        next(newHtml);
      });
});

async function doMatchReplacementsAsync(html, matches, methods) {
  let mod = 0;
  for (const match of matches) {
    try {
      let start = match.index + mod;
      let size = match[0].length;
      let path = match[1];

      let res = await fetch(path);
      
      if (!res.ok) {
        continue;
      }

      let data = await res.text();

      try {
        let json = JSON.stringify(JSON.parse(data));

        if (match[2]) {
          json = methods[match[2]](json);
        }

        mod += json.length - size;

        html = html.slice(0, start) + json + html.slice(start + size);
      } catch (error) {
        if (match[2]) {
          data = methods[match[2]](data);
        }

        mod += data.length - size;

        html = html.slice(0, start) + data + html.slice(start + size);
      }
    } catch (error) {
      console.log('Failed to replace file path: ' + error);
    }
  }

  return html;
}

export { fileNameReplacePlugin };
