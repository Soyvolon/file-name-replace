import { fileNameReplacePlugin } from "./plugin";

const docsify = window.$docsify || {};

const props =
  {
    fileNameReplacePlugin: docsify.fileNameReplacePlugin ?? {}
  } || {};

docsify.plugins = [].concat(
  docsify.plugins || [],
  fileNameReplacePlugin(props.fileNameReplacePlugin)
);
