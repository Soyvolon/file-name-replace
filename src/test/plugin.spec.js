import { test, expect } from "@playwright/test";
import { promises as fs } from "fs";

test.describe("README.md should be rendered", () => {
  test('page should have title of "Docsify plugin playground"', async ({
    page,
  }) => {
    await page.goto("/");
    const title = await page.title();
    expect(title).toBe("Docsify plugin playground");
  });
});

test.describe("JSON file data replace should work.", () => {
  test("Runkit test divs should exist.", async ({ page }) => {
    await page.goto('/');

    let count = 2;

    for (let i = 1; i <= count; i++) {
      const testDiv = await page.locator(`#runkit-test-${i}`);
      expect(testDiv).toBeDefined();
    }
  }),

  test("Runkit test divs should have proper data.", async ({ page }) => {
    await page.goto('/');

    let count = 2;

    for (let i = 1; i <= count; i++) {
      let testJson = JSON.parse(await fs.readFile(`src/runkit-test-${i}.json`));

      const testDiv = await page.locator(`#runkit-test-${i}`);
      let envData = await testDiv.getAttribute('data-runkit-environment');
      let envJson = JSON.parse(envData);

      expect(JSON.stringify(envJson)).toMatch(JSON.stringify(testJson));
    }
  })
})

test.describe("Plain file data replace should work.", () => {
  test("Plain test divs should exist.", async ({ page }) => {
    await page.goto('/');

    let count = 1;

    for (let i = 1; i <= count; i++) {
      const testDiv = await page.locator(`#plain-test-${i}`);
      expect(testDiv).toBeDefined();
    }
  }),

  test("Plain test divs should have proper data.", async ({ page }) => {
    await page.goto('/');

    let count = 1;

    for (let i = 1; i <= count; i++) {
      let testData = await fs.readFile(`src/plain-test-${i}.txt`, 'utf-8');

      const testDiv = await page.locator(`#plain-test-${i}`);
      let envData = await testDiv.textContent();

      // make sure to clean some extra whitespace, we dont really care about that.
      expect(envData.split('\n').map(e => e.trim()).join('\n'))
        .toMatch(testData.split('\n').map(e => e.trim()).join('\n'));
    }
  })
})