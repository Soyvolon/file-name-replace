# Headline

> A super awesome template to develop your own Docsify plugin from scratch!

Docsify generates your documentation website on the fly. Unlike GitBook, it does not generate static html files. Instead, it smartly loads and parses your Markdown files and displays them as a website. To start using it, all you need to do is create an index.html and deploy it on GitHub Pages.

<div
    id="runkit-test-1"
  data-runkit
  data-runkit-environment='{{r:runkit-test-1.json}}'
>
</div>
<div
    id="runkit-test-2"
  data-runkit
  data-runkit-environment='{{r:runkit-test-2.json}}'
>
</div>

<div
    id="runkit-test-3"
  data-runkit
  data-runkit-environment='{{r:runkit-test-3.json}:valueToString}'
>
</div>

<div
  id="plain-test-1"
>
{{r:plain-test-1.txt}}
</div>