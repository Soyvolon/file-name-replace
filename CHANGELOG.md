# 1.2.0

+ added custom method support

In files: `{{r:path\to\file.json}:nameOfMethod}`

In the config:

```js
window.$docsify = {
    fileNameReplacePlugin: {
        methods: {
            valueToString(json) {
                jobj = JSON.parse(json);

                let val = jobj['value'];

                val = JSON.stringify(val);
                
                let data = JSON.stringify({
                    name: json['name'],
                    value: val
                });

                return data;
            }
        }
    }
};
```
