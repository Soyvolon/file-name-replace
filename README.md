# file-name-replace

Replaces file names with the actual data they contain to keep large chunks of data cleaner.

Looks for `{{r:<file_path.file_ext>}}` to replace with the contents from the file path. The file
must be in the documentation folder as well.

Current version:
```html
<script src="https://cdn.jsdelivr.net/npm/@soyvolon/docsify-file-name-replace/dist/plugin.js"></script>
```
